# Terraform script for sigma infrastructure

### 1. Put aws credential to the provider in variables.tf safely (No Plaintext)

### 2. Backend is commented so that you can just create tfstate locally, otherwise:
- Create a s3 bucket and dynamodb table, put the s3 bucket, path to tfstate and dynamodb_table name in backend.tf config, uncomment the code

### 3. Run commands
```bash
# Initialize terraform & install dependencies
terraform init

# Formated the code automatically
terraform fmt

terraform plan
terraform apply

terraform destroy
```