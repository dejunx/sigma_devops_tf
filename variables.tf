provider "aws" {
  access_key  = ""
  secret_key = ""
  region      = var.aws_region
}

variable "aws_region" {
  type        = string
  default     = "ap-southeast-2"
  description = "description"
}

variable "environment" {
  type        = string
  default     = "uat"
  description = "description"
}

variable "name_prefix" {
  type        = string
  default     = "sigma"
  description = "description"
}

variable "vpc_cidr_block" {
  type = string
  default = "10.0.0.0/16"
}
