resource "aws_vpc" "sigma_vpc" {
  cidr_block           = "${var.vpc_cidr_block}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "${var.name_prefix}-app"
  }
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.sigma_vpc.id
  tags = {
    Name = "${var.name_prefix}-app"
  }
}